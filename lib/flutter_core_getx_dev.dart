library flutter_core_getx_dev;

import 'package:flutter_core_getx_dev/app/data/storage.dart';
import 'package:flutter_plugin_gallery/flutter_plugin_gallery.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

import 'app/services/core_up_download_service/up_download_package.dart';

export 'package:flutter_cache_manager/flutter_cache_manager.dart';
export 'package:flutter_localizations/flutter_localizations.dart';
export 'package:flutter_plugin_camera/flutter_plugin_camera.dart';
export 'package:flutter_plugin_gallery/flutter_plugin_gallery.dart';
export 'package:get/get.dart';
export 'package:pedantic/pedantic.dart';
export 'package:permission_handler/permission_handler.dart';
export 'package:permission_handler/permission_handler.dart';
export 'package:photo_manager/photo_manager.dart';

export 'app/data/storage.dart';
export 'app/models/device_info_model.dart';
export 'app/services/cache_manager_custom.dart';
export 'app/services/core_up_download_service/up_download_package.dart';
export 'app/services/media_picker.dart';
export 'app/utils/extension.dart';
export 'app/widget/app_bar_widget.dart';
export 'app/widget/avatar_circle_widget.dart';
export 'app/widget/bottom_sheet_with_action_widget.dart';
export 'app/widget/disable_glow_listview_widget.dart';
export 'app/widget/download_file_widget.dart';
export 'app/widget/image_network_widget.dart';
export 'app/widget/no_data_widget.dart';

class CoreConfig {
  static Future<void> init({required String appNameVietTat}) async {
    await GetStorage.init(appNameVietTat).then((value) {
      Storage.init(appNameVietTat);
    });

    await LocalNotifications.instance.init();
  }

  static Future<void> initServices() async {
    Get.create(() => GalleryController());
  }
}
