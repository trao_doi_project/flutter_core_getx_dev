import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_core_getx_dev/app/data/storage.dart';
import 'package:flutter_core_getx_dev/app/services/core_up_download_service/up_download_package.dart';
import 'package:flutter_core_getx_dev/app/utils/strings.dart';
import 'package:flutter_core_getx_dev/app/utils/extension.dart';
import 'package:path/path.dart' as path;

class CoreHttp {
  static final CoreHttp instance = CoreHttp._();
  final int _timeOut = 30000; //30 s
  late Dio _dio;
  CoreHttp._() {
    // httpClient.baseUrl = baseURL;
    final BaseOptions options = BaseOptions(
        connectTimeout: _timeOut,
        receiveTimeout: _timeOut,
        contentType: ContentType.json.value,
        responseType: ResponseType.plain);

    _dio = Dio(options);

    // httpClient.timeout = const Duration(seconds: 15);
  }

  Future<dynamic> getAsync(String url, {Map<String, String>? headers}) async {
    try {
      if (headers.isNullOrEmpty())
        headers = {
          'Authorization': Storage.instance.readString(apiAuthBearerOrBasic)
        };

      final response = await _dio.get(url, options: Options(headers: headers));
      if (response.statusCode == 200 && response.data != null) {
        print(response.data);
        return jsonDecode(response.data);
      }
      return null;
    } catch (e) {
      print('getAsync _ ' + e.toString());
      return null;
    }
  }

  Future<dynamic> postAsync(String url, Map<String, dynamic> param,
      {Map<String, String>? headers}) async {
    try {
      if (headers.isNullOrEmpty())
        headers = {
          'Authorization': Storage.instance.readString(apiAuthBearerOrBasic)
        };

      final response = await _dio.post(url,
          data: jsonEncode(param), options: Options(headers: headers));
      if (response.statusCode == 200 && response.data != null) {
        print(response.data);
        return jsonDecode(response.data);
      }
      return null;
    } catch (e) {
      print('postAsync _ ' + e.toString());
      return null;
    }
  }

  Future<File?> download(String url, savePath) async {
    try {
      final HttpClient _httpClient = HttpClient();
      final HttpClientRequest _request =
          await _httpClient.getUrl(Uri.parse(url));
      final HttpClientResponse _response = await _request.close();
      if (_response.statusCode == 200) {
        print(_response);
        final Uint8List _bytes =
            await consolidateHttpClientResponseBytes(_response);
        final File _newFile = File(savePath);
        return await _newFile.writeAsBytes(_bytes);
      }
      return null;
    } catch (e) {
      print('download _ ' + e.toString());
      return null;
    }
  }

  Future<dynamic> postUploadFile(String url,
      {required List<File> files,
      Map<String, String>? headers,
      Function(double percent)? callbackProcess}) async {
    try {
      if (headers.isNullOrEmpty())
        headers = {
          'Authorization': Storage.instance.readString(apiAuthBearerOrBasic)
        };

      // final List<MapEntry<String, MultipartFile>> _uploadData = files
      //     .map((e) => MapEntry(
      //         "files", MultipartFile(e.path, filename: path.basename(e.path))))
      //     .toList();

      final List<MapEntry<String, MultipartFile>> _uploadData = files
          .map((e) => MapEntry(
              'files',
              MultipartFile.fromFileSync(e.path,
                  filename: path.basename(e.path))))
          .toList();

      final Response response = await _dio.post(
        url,
        data: FormData()..files.addAll(_uploadData),
        options: Options(headers: headers),
        onSendProgress: (int sent, int total) {
          final double _percent = (sent / total);
          callbackProcess?.call(_percent);
        },
      );
      if (response.statusCode == 200 && response.data != null) {
        print(response.data);
        return jsonDecode(response.data);
      }
      return null;
    } catch (e) {
      return null;
    }
  }

  Future<String> uploadBackgroud(String url, List<File> files) async {
    final String _taskID = await UploadFile.instance.uploadWithMultipart(
        url, files, headers: {
      'Authorization': Storage.instance.readString(apiAuthBearerOrBasic)
    });
    return _taskID;
  }

  Future<String?> download1(String url, savePath,
      {void Function(int count, int total)? callBack}) async {
    final CancelToken cancelToken = CancelToken();
    try {
      await _dio.download(url, savePath,
          onReceiveProgress: callBack, cancelToken: cancelToken);
      return savePath;
    } catch (e) {
      print(e);
      return null;
    }
  }

  Future<File?> download2(String url,
      {required Map<String, dynamic> param,
      required String filePath,
      void Function(int count, int total)? callBack}) async {
    try {
      final Response response = await _dio.post(
        url,
        data: jsonEncode(param),
        onReceiveProgress: callBack,
        options: Options(
            responseType: ResponseType.bytes,
            followRedirects: false,
            receiveTimeout: 0),
      );
      final File file = File(filePath);
      final raf = file.openSync(mode: FileMode.write);
      raf.writeFromSync(response.data);
      await raf.close();
      return file;
    } catch (e) {
      return null;
    }
  }
}
