class DeviceInfoModel {
  final String deviceID;
  final String platform;
  final String model;
  final String manufacturer;
  final String version;

  DeviceInfoModel({
    this.deviceID = '',
    this.platform = '',
    this.model = '',
    this.manufacturer = '',
    this.version = '',
  });
}
