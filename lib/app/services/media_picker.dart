import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:open_file/open_file.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:path/path.dart' as path;
import 'package:mime_type/mime_type.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:video_compress/video_compress.dart';
import 'package:video_thumbnail/video_thumbnail.dart';

class MediaPicker {
  MediaPicker._();
  static final MediaPicker instance = MediaPicker._();

  factory MediaPicker() => instance;

  final Subscription videoCompressSubscription =
      VideoCompress.compressProgress$.subscribe((event) {});

  static const List<String> _extentions = [
    'txt',
    'xlsx',
    'xls',
    'pdf',
    'doc',
    'ppt',
    'pptx',
    'docx'
  ];

  Future<List<File>?> pickFile(
      {bool multiFile = false,
      bool media = false,
      List<String> allowedExtensions = _extentions}) async {
    if (await Permission.storage.request().isGranted) {
      final FilePickerResult? result = await FilePicker.platform.pickFiles(
        type: FileType.custom,
        allowMultiple: multiFile,
        allowedExtensions: allowedExtensions,
      );
      if (result != null) {
        final List<File> files =
            result.paths.map((path) => File(path!)).toList();
        return files;
      }
    }
    return null;
  }

  Future<List<File>?> pickFileVideo({bool multiFile = false}) async {
    if (await Permission.storage.request().isGranted) {
      final FilePickerResult? result = await FilePicker.platform.pickFiles(
          type: FileType.video,
          allowMultiple: multiFile,
          allowCompression: true);
      if (result != null) {
        return result.paths.map((e) => File(e!)).toList();
      }
    }
    return null;
  }

  Future<List<File>> compressMedia<T>(List<File> data,
      {bool isThumb = false,
      String urlUpload = '',
      int quality = 50,
      int imagePercentage = 80}) async {
    try {
      final List<File> _tempFile = <File>[];
      for (final File i in data) {
        final String? mimeType = mime(path.basename(i.path));
        if (mimeType != null) {
          if (mimeType.contains('image/')) {
            _tempFile.add(await compressImage(i.path, path.basename(i.path),
                quality: quality));
            if (isThumb)
              _tempFile.add(await MediaPicker.instance.compressImage(
                  i.path, 'thumbnailChat_${path.basename(i.path)}',
                  quality: 15));
          } else if (mimeType.contains('video/')) {
            final File? _tempPath =
                (i.lengthSync() > 31457280) ? await _videoCompress(i.path) : i;
            if (_tempPath != null) {
              if (isThumb) {
                final File _thumb = await getVideoThumb(i.path);
                _tempFile.add(_thumb);
              }
              _tempFile.add(_tempPath);
            } else {
              if (isThumb) {
                final File _thumb = await getVideoThumb(i.path);
                _tempFile.add(_thumb);
              }
              _tempFile.add(i);
            }
          } else {
            _tempFile.add(i);
          }
        } else
          _tempFile.add(i);
      }

      return _tempFile;
    } catch (e) {
      print(e);
      return data;
    }
  }

  Future<File> getVideoThumb(String filePath) async {
    final fileName = await VideoThumbnail.thumbnailFile(
      video: filePath,
      thumbnailPath: (await getTemporaryDirectory()).path,
      imageFormat: ImageFormat.JPEG,
      quality: 20,
    );
    return File(fileName!);
  }

  Future<File> compressImage(String pathFile, String fileName,
      {int quality = 50, int width = 480, int height = 720}) async {
    final Directory tempDir = await getTemporaryDirectory();
    final String tempPath = path.join(tempDir.path, 'compress');
    Directory(tempPath).createSync(recursive: true);
    final String newPath = path.join(tempPath, fileName);

    if (!fileName.toLowerCase().endsWith('.jpg') ||
        !fileName.toLowerCase().endsWith('.jpeg') ||
        !fileName.toLowerCase().endsWith('.png') ||
        !fileName.toLowerCase().endsWith('.webp') ||
        !fileName.toLowerCase().endsWith('.heic'))
      return File(pathFile).copy(newPath);

    final File? result = await FlutterImageCompress.compressAndGetFile(
        pathFile, newPath,
        minHeight: height,
        minWidth: width,
        quality: quality,
        format: _compressFormat(path.extension(fileName.toLowerCase())));
    return result ?? File(pathFile);
  }

  CompressFormat _compressFormat(String extension) {
    switch (extension) {
      case '.jpg':
      case '.jpeg':
        return CompressFormat.jpeg;
      case '.png':
        return CompressFormat.png;
      case '.heic':
        return CompressFormat.heic;
      default:
        return CompressFormat.webp;
    }
  }

  Future<File?> _videoCompress(String videoPath) async {
    final MediaInfo? mediaInfo = await VideoCompress.compressVideo(
      videoPath,
      quality: VideoQuality.MediumQuality,
      deleteOrigin: false, // It's false by default
    );

    if (mediaInfo != null) return mediaInfo.file;
    return null;
  }

  Future<OpenResult> openFile(String filePath) async {
    final OpenResult result = await OpenFile.open(filePath);
    return result;
  }

  Future<void> openUrl(String url) async {
    await launch(
      url,
      forceSafariVC: true,
      forceWebView: true,
      enableJavaScript: true,
    );
  }

  Future<void> makePhoneCall(String phoneNumber) async {
    if (await canLaunch(phoneNumber)) {
      await launch(phoneNumber);
    } else {
      throw 'Could not launch $phoneNumber';
    }
  }
}
