import 'dart:async';
import 'dart:io';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

class CoreConnectivityService {
  bool isShowingDialog = false;
  final StreamController<bool> hasConnection =
      StreamController<bool>.broadcast();

  late StreamSubscription<ConnectivityResult> _connectivitySubscription;
  late ConnectivityResult _result;
  final Connectivity _connectivity = Connectivity();

  CoreConnectivityService._() {
    _initConnectivity();
  }

  static final CoreConnectivityService instance = CoreConnectivityService._();

  void showSnackBar() {
    Get.snackbar('Thông báo', 'thiết bị mất kết nối internet');
  }

  Future<bool> checkConnection() async {
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        return true;
      } else {
        return false;
      }
    } on SocketException catch (_) {
      return false;
    }
  }

  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    switch (result) {
      // case ConnectivityResult.wifi:
      // case ConnectivityResult.mobile:
      //   // if (!isFirstLoad) await SignalR.instance!.startServer(isConnect: true);
      //   break;
      case ConnectivityResult.none:
        showSnackBar();
        hasConnection.sink.add(false);
        break;
      default:
        hasConnection.sink.add(true);
        break;
    }
  }

  Future<void> _initConnectivity() async {
    try {
      _result = await _connectivity.checkConnectivity();
    } on PlatformException catch (e) {
      print(e.toString());
    }
    _connectivitySubscription =
        _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);
  }

  void dispose() {
    _connectivitySubscription.cancel();
  }
}
