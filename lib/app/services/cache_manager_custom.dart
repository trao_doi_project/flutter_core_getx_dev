import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:pedantic/pedantic.dart';

class CacheManagerCustom {
  CacheManagerCustom._();
  static final CacheManagerCustom instance = CacheManagerCustom._();

  final BaseCacheManager _cacheManager = DefaultCacheManager();

  Future<FileInfo?> getFileInCache(String fileURL) async {
    try {
      final FileInfo? fileInfo = await _cacheManager.getFileFromCache(fileURL);
      if (fileInfo == null) {
        unawaited(_cacheManager.downloadFile(fileURL));
        return fileInfo;
      } else {
        return fileInfo;
      }
    } catch (e) {
      print(e);
      return null;
    }
  }

  Future<FileInfo?> getFileAwaitInCache(String fileURL) async {
    try {
      final FileInfo? fileInfo = await _cacheManager.getFileFromCache(fileURL);
      if (fileInfo == null) {
        return await _cacheManager.downloadFile(fileURL);
      } else {
        return fileInfo;
      }
    } catch (e) {
      print(e);
      return null;
    }
  }

  Future<List<FileInfo>?> getListFileAwaitInCache(List<String> fileURLs) async {
    try {
      return await Future.wait(fileURLs.map((e) async {
        final FileInfo? fileInfo = await _cacheManager.getFileFromCache(e);
        if (fileInfo == null) {
          return await _cacheManager.downloadFile(e);
        } else {
          return fileInfo;
        }
      }).toList());
    } catch (e) {
      print(e);
      return null;
    }
  }
}
