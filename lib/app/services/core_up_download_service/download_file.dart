part of 'up_download_package.dart';

class DownloadFile extends GetxService {
  DownloadFile._();
  static final DownloadFile instance = DownloadFile._();

  Future<String?> downLoadFile(String url, String filePath,
      {void callBack(int count, int total)?}) async {
    try {
      return await CoreHttp.instance
          .download1(url, filePath, callBack: callBack);
    } catch (e) {
      print(e);
      return null;
    }
  }

  Future<CreateFolderModel> checkAndCreateDownloadFolder(
      String appFolder, String fileName) async {
    String filePath = '';
    Directory? sysPath;
    if (await Permission.storage.request().isGranted) {
      if (Platform.isAndroid) {
        filePath = join('/storage/emulated/0/Download/$appFolder', fileName);
      } else {
        sysPath = await path_provider.getDownloadsDirectory();
        filePath = join('${sysPath?.path}/$appFolder', fileName);
      }

      if (!File(filePath).existsSync()) {
        if (Platform.isAndroid)
          sysPath = Directory('/storage/emulated/0/Download/$appFolder');
        else {
          final Directory iosDir = await getApplicationDocumentsDirectory();
          sysPath = Directory('${iosDir.path}/$appFolder');
        }
        if (sysPath.existsSync())
          return CreateFolderModel(isExists: false, filePath: sysPath.path);
        else {
          final Directory newFolder = await sysPath.create(recursive: true);
          return CreateFolderModel(isExists: false, filePath: newFolder.path);
        }
      } else
        return CreateFolderModel(isExists: true, filePath: filePath);
    }
    return CreateFolderModel(isExists: false, filePath: filePath);
  }

  Future<CreateFolderModel> checkAndCreateCacheFolder(String fileName,
      {String appFolder = ''}) async {
    final Directory _systemPath = await path_provider.getTemporaryDirectory();
    final String _filePath = join(_systemPath.path, appFolder, fileName);
    if (File(_filePath).existsSync())
      return CreateFolderModel(isExists: true, filePath: _filePath);
    return CreateFolderModel(isExists: false, filePath: _filePath);
  }
}

class CreateFolderModel {
  CreateFolderModel({
    required this.isExists,
    required this.filePath,
  });

  final bool isExists;
  final String filePath;

  factory CreateFolderModel.fromMap(Map<String, dynamic> json) =>
      CreateFolderModel(
        isExists: json["isExists"] ?? false,
        filePath: json["filePath"] ?? '',
      );

  Map<String, dynamic> toMap() => {
        "isExists": isExists,
        "filePath": filePath,
      };
}
