part of 'up_download_package.dart';

enum TaskUpload { Cancel, CancelAll, ClearUploads }

class UploadFile extends GetxController {
  RxInt percent = 0.obs;

  UploadFile._() {
    // FlutterUploader().setBackgroundHandler(_backgroundHandler);
    _backgroundHandler();
  }
  static final UploadFile instance = UploadFile._();

  late final FlutterUploader _uploader = FlutterUploader();

  late Stream<UploadTaskProgress> uploadProgress;
  late Stream<UploadTaskResponse> uploadResult;

  String _taskID = '';

  void _backgroundHandler() {
    // Needed so that plugin communication works.
    // WidgetsFlutterBinding.ensureInitialized();

    // This uploader instance works within the isolate only.
    // You have now access to:
    uploadProgress = _uploader.progress;
    uploadResult = _uploader.result;

    _uploader.progress.listen((progress) {
      // upload progress
      _taskID = progress.taskId;

      LocalNotifications.instance.notificationProgress(progress.taskId.hashCode,
          progress.progress! > 0 ? progress.progress! : 0);
      print(progress.progress);
      // prinprogress.t('progress: ${progress.progress}');
      percent.value = progress.progress ?? 0;
    });

    _uploader.result.listen((result) {
      // print('result: ${result.statusCode}');
      LocalNotifications.instance.cleanNotifications(result.taskId.hashCode);

      if (result.taskId == _taskID) percent.value = 100;
    });
  }

  Future<String> uploadWithMultipart(String urlUpload, List<File> files,
      {required Map<String, String> headers}) async {
    try {
      final taskId = await _uploader.enqueue(
        MultipartFormDataUpload(
          url: urlUpload, //required: url to upload to
          files:
              files.map((e) => FileItem(path: e.path, field: 'files')).toList(),
          // files: [
          //   FileItem(path: '/path/to/file', fieldname: "file")
          // ], // required: list of files that you want to upload
          method: UploadMethod.POST, // HTTP method  (POST or PUT or PATCH)
          headers: headers,
          // data: {"name": "john"}, // any data you want to send in upload request
          // tag: 'my tag', // custom tag which is returned in result/progress
        ),
      );

      return taskId;
    } catch (e, s) {
      return '';
    }
  }

  void taskUpload(TaskUpload task, {required String taskId}) {
    switch (task) {
      case TaskUpload.Cancel:
        // TODO: Handle this case.
        _uploader.cancel(taskId: taskId);
        break;
      case TaskUpload.CancelAll:
        // TODO: Handle this case.
        _uploader.cancelAll();
        break;
      case TaskUpload.ClearUploads:
        // TODO: Handle this case.
        _uploader.clearUploads();
        break;
    }
  }
}
