part of 'up_download_package.dart';

class UploadProcessIndicator extends StatelessWidget {
  final UploadFile _uploadFile = UploadFile.instance;

  final String taskID;
  final double lineHeight;
  final bool isShowPercent;
  final double percentSize;
  final Color percentColor;
  final Color backgroundColor;
  final Color progressColor;

  UploadProcessIndicator(
      {Key? key,
      required this.taskID,
      this.lineHeight = 5,
      this.isShowPercent = false,
      this.percentSize = 10,
      this.percentColor = Colors.black,
      this.backgroundColor = Colors.transparent,
      this.progressColor = Colors.greenAccent})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Obx(() => (_uploadFile.percent.value > 0 &&
              _uploadFile.percent.value < 100)
          ? LinearPercentIndicator(
              lineHeight: lineHeight,
              percent: _uploadFile.percent.value / 100,
              center: isShowPercent
                  ? Text(
                      '${_uploadFile.percent.value.toString()} %',
                      style:
                          TextStyle(fontSize: percentSize, color: percentColor),
                    )
                  : null,
              // trailing: Icon(Icons.close),
              linearStrokeCap: LinearStrokeCap.roundAll,
              backgroundColor: backgroundColor,
              progressColor: progressColor,
            )
          : const SizedBox.shrink()),
    );
  }
}
