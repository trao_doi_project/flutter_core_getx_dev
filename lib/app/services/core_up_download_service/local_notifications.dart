part of 'up_download_package.dart';

class LocalNotifications extends GetxService {
  LocalNotifications._();
  static final LocalNotifications instance = LocalNotifications._();
  final flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();
  String? selectedNotificationPayload;

  final AndroidNotificationChannel _channel = const AndroidNotificationChannel(
    '02862582324', // id
    'VietInfo', // title
    'VietInfo', // description
    importance: Importance.max,
    enableLights: true,
    enableVibration: true,
    playSound: true,
  );

  Future init() async {
    const AndroidInitializationSettings initializationSettingsAndroid =
        AndroidInitializationSettings('@mipmap/ic_launcher');

    const InitializationSettings initializationSettings =
        InitializationSettings(
            android: initializationSettingsAndroid,
            iOS: IOSInitializationSettings());
    await flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: (String? payload) async {
      if (payload != null) {
        print(payload);
      }
    });
  }

  Future notificationProgress(int id, int percent,
      {String title = 'Đang tải lên...', String? body}) async {
    final AndroidNotificationDetails androidPlatformChannelSpecifics =
        AndroidNotificationDetails(
            _channel.id, _channel.name, _channel.description,
            channelShowBadge: false,
            importance: Importance.defaultImportance,
            priority: Priority.high,
            onlyAlertOnce: true,
            showProgress: true,
            maxProgress: 100,
            progress: percent);
    final NotificationDetails platformChannelSpecifics = NotificationDetails(
        android: androidPlatformChannelSpecifics,
        iOS: const IOSNotificationDetails(
            presentBadge: true, presentAlert: true, presentSound: true));
    await flutterLocalNotificationsPlugin.show(
        id, '$title $percent %', body, platformChannelSpecifics,
        payload: 'item x');
  }

  Future cleanNotifications(int id) async {
    await flutterLocalNotificationsPlugin.cancel(id);
  }
}
