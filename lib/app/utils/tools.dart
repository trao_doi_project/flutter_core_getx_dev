import 'package:device_info/device_info.dart';
import 'package:flutter/material.dart';
import 'package:flutter_core_getx_dev/app/models/device_info_model.dart';
import 'package:get/get.dart';

Future<DeviceInfoModel> deviceInfo() async {
  final DeviceInfoPlugin _deviceInfo = DeviceInfoPlugin();
  if (GetPlatform.isAndroid) {
    final AndroidDeviceInfo _androidInfo = await _deviceInfo.androidInfo;
    return DeviceInfoModel(
        deviceID: _androidInfo.androidId,
        manufacturer: _androidInfo.manufacturer,
        model: _androidInfo.model,
        platform: 'Android',
        version: _androidInfo.version.baseOS ?? '');
  } else {
    final IosDeviceInfo _iosInfo = await _deviceInfo.iosInfo;
    return DeviceInfoModel(
        deviceID: _iosInfo.identifierForVendor,
        manufacturer: _iosInfo.utsname.machine,
        model: _iosInfo.model,
        platform: 'iOS',
        version: _iosInfo.systemVersion);
  }
}

RichText richTextSearch(String source, String query,
    {TextStyle? styleQuery, TextStyle? styleRichText}) {
  if (query.isEmpty || !source.toLowerCase().contains(query.toLowerCase())) {
    return RichText(
      text: TextSpan(
        children: [TextSpan(text: source)],
        style: styleRichText,
      ),
    );
  }
  final matches = query.toLowerCase().allMatches(source.toLowerCase());

  int lastMatchEnd = 0;

  final List<TextSpan> children = [];
  for (var i = 0; i < matches.length; i++) {
    final match = matches.elementAt(i);

    if (match.start != lastMatchEnd) {
      children.add(TextSpan(
        text: source.substring(lastMatchEnd, match.start),
      ));
    }

    children.add(TextSpan(
      text: source.substring(match.start, match.end),
      style: styleQuery ??
          const TextStyle(fontWeight: FontWeight.bold, color: Colors.red),
    ));

    if (i == matches.length - 1 && match.end != source.length) {
      children.add(TextSpan(
        text: source.substring(match.end, source.length),
      ));
    }

    lastMatchEnd = match.end;
  }
  return RichText(
    text: TextSpan(
      children: children,
      style: styleRichText,
    ),
  );
}

void showSnackBar(String noiDung,
    {String title = '',
    Widget? icon,
    Color textColor = Colors.white,
    Duration? thoiGian,
    SnackPosition? position}) {
  Get.snackbar(title, noiDung,
      colorText: textColor,
      barBlur: 50,
      duration: thoiGian ?? const Duration(seconds: 2),
      snackPosition: position,
      backgroundColor: Colors.green,
      icon: icon);
}
