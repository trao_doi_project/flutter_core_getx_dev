import 'package:intl/intl.dart';

extension CheckData<T> on T {
  bool isNullOrEmpty() {
    if (this == null ||
        this == '' ||
        this == 'null' ||
        this == [] ||
        this == {} ||
        this == ' ') return true;
    return false;
  }

  bool isNotNullEmpty() {
    if (this == null ||
        this == '' ||
        this == 'null' ||
        this == [] ||
        this == {} ||
        this == ' ') return false;
    return true;
  }
}

extension StringUtil on String {
  String xoaDau() {
    const String _vietnamese = 'aAeEoOuUiIdDyY';
    final List<RegExp> _vietnameseRegex = <RegExp>[
      RegExp(r'à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ'),
      RegExp(r'À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ'),
      RegExp(r'è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ'),
      RegExp(r'È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ'),
      RegExp(r'ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ'),
      RegExp(r'Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ'),
      RegExp(r'ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ'),
      RegExp(r'Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ'),
      RegExp(r'ì|í|ị|ỉ|ĩ'),
      RegExp(r'Ì|Í|Ị|Ỉ|Ĩ'),
      RegExp(r'đ'),
      RegExp(r'Đ'),
      RegExp(r'ỳ|ý|ỵ|ỷ|ỹ'),
      RegExp(r'Ỳ|Ý|Ỵ|Ỷ|Ỹ')
    ];
    String result = this;
    if (result is String) {
      for (int i = 0; i < _vietnamese.length; ++i) {
        result = result.replaceAll(_vietnameseRegex[i], _vietnamese[i]);
      }
    }
    return result;
  }

  bool isEmail() {
    const String pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    final RegExp regex = RegExp(pattern);
    if (!regex.hasMatch(this))
      return false;
    else
      return true;
  }

  bool isPhoneNumber() {
    const String pattern = r'^[a-zA-Z0-9]+$';
    final RegExp regex = RegExp(pattern);
    if (!regex.hasMatch(this))
      return false;
    else
      return true;
  }

  bool isLink() {
    final RegExp urlPattern = RegExp(
        r'(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)',
        caseSensitive: false);
    return urlPattern.hasMatch(this);
  }

  bool isYoutube() {
    final RegExp youtubePattern = RegExp(
        r'^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$');
    return youtubePattern.hasMatch(this);
  }
}

extension DateTimeToString on DateTime {
  String format(String str) {
    final formatter = DateFormat(str);
    final String formatted = formatter.format(this);
    return formatted;
  }
}
