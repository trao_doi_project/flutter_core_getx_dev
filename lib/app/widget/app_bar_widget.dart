import 'package:flutter/material.dart';

class AppBarWidget extends StatelessWidget with PreferredSizeWidget {
  final Text title;
  final bool isTop;
  final Color? backgroundColor;
  final Widget? iconButton1;
  final Widget? iconButton2;
  final Widget? iconLeading;
  AppBarWidget(
      {required this.title,
      this.isTop = false,
      this.iconButton1,
      this.iconButton2,
      this.iconLeading,
      this.backgroundColor});

  @override
  Widget build(BuildContext context) {
    return AppBar(
      iconTheme: const IconThemeData(color: Colors.white),
      leading: iconLeading,
      title: title,
      centerTitle: true,
      backgroundColor: backgroundColor,
      elevation: 0,
      actions: (iconButton1 != null)
          ? <Widget>[
              iconButton1!,
              if (iconButton2 != null) iconButton2! else const SizedBox.shrink()
            ]
          : null,
    );
  }

  @override
  Size get preferredSize => const Size.fromHeight(kToolbarHeight);
}
