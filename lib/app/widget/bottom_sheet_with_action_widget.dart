import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import '../../flutter_core_getx_dev.dart';

class BottomSheetWithActionWidget extends StatelessWidget {
  final List<BottomSheetItem> bottomItems;
  final ValueChanged<int> idValue;

  const BottomSheetWithActionWidget(
      {Key? key, required this.bottomItems, required this.idValue})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        height: Get.height * 0.12,
        decoration: BoxDecoration(
            color: Colors.white, borderRadius: BorderRadius.circular(5)),
        child: Padding(
          padding: const EdgeInsets.all(10),
          child: GridView.builder(
            physics: const NeverScrollableScrollPhysics(),
            itemCount: bottomItems.length,
            shrinkWrap: true,
            gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2,
              mainAxisSpacing: 5,
              crossAxisSpacing: 5,
            ),
            itemBuilder: (context, index) {
              final BottomSheetItem _item = bottomItems[index];
              return GestureDetector(
                onTap: () => idValue(_item.id),
                child: Column(
                  children: [
                    Image.asset(
                      _item.image,
                      width: 50,
                    ),
                    Text(_item.title)
                  ],
                ),
              );
            },
          ),
        ));
  }
}

class BottomSheetItem {
  String title;
  String image;
  int id;
  BottomSheetItem({required this.title, required this.id, required this.image});
}
