import 'package:flutter/material.dart';

class NoDataWidget extends StatelessWidget {
  final String? noiDung;
  const NoDataWidget({this.noiDung});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text(
        noiDung ?? 'Không có dữ liệu',
        style: const TextStyle(color: Colors.blue),
      ),
    );
  }
}
