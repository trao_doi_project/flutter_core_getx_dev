import 'package:flutter/material.dart';
import 'package:extended_image/extended_image.dart';
import '../utils/extension.dart';

class ImageNetworkWidget extends StatelessWidget {
  final double? imageWidth;
  final double? imageHeight;
  final String imageAssetDefault;
  final String? urlImageOffline;
  final String? urlImageNetwork;
  final BoxFit? fit;
  final bool isOnline;
  final bool isOnTapReload;
  final BoxShape? shape;
  final BorderRadius? borderRadius;
  final String? packageName;

  const ImageNetworkWidget(
      {this.urlImageNetwork,
      required this.imageAssetDefault,
      this.urlImageOffline,
      this.imageHeight,
      this.isOnline = true,
      this.imageWidth,
      this.fit = BoxFit.cover,
      this.isOnTapReload = true,
      this.packageName,
      this.shape = BoxShape.rectangle,
      this.borderRadius});

  @override
  Widget build(BuildContext context) {
    if (isOnline) {
      if (urlImageNetwork.isNullOrEmpty())
        return Image.asset(
          imageAssetDefault,
          fit: fit,
          width: imageWidth,
          height: imageWidth,
          package: packageName,
        );
      return ExtendedImage.network(
        urlImageNetwork!,
        shape: shape,
        borderRadius: borderRadius,
        loadStateChanged: (ExtendedImageState state) {
          switch (state.extendedImageLoadState) {
            case LoadState.completed:
              return ExtendedRawImage(
                image: state.extendedImageInfo?.image,
                fit: fit,
                width: imageWidth,
                height: imageWidth,
              );
            case LoadState.failed:
              return GestureDetector(
                child: Image.asset(imageAssetDefault,
                    fit: fit,
                    width: imageWidth,
                    height: imageWidth,
                    package: packageName),
                onTap: (isOnTapReload)
                    ? () {
                        state.reLoadImage();
                      }
                    : null,
              );
            default:
              return Image.asset(imageAssetDefault,
                  fit: fit,
                  width: imageWidth,
                  height: imageWidth,
                  package: packageName);
          }
        },
      );
    }
    if (urlImageOffline.isNullOrEmpty())
      return Image.asset(imageAssetDefault,
          fit: fit,
          width: imageWidth,
          height: imageWidth,
          package: packageName);
    return ExtendedImage.file(
      File(urlImageOffline!),
      fit: fit,
      shape: shape,
      borderRadius: borderRadius,
      loadStateChanged: (ExtendedImageState state) {
        switch (state.extendedImageLoadState) {
          case LoadState.completed:
            return ExtendedRawImage(
              image: state.extendedImageInfo?.image,
              fit: fit,
              width: imageWidth,
              height: imageWidth,
            );
          case LoadState.failed:
            return GestureDetector(
              child: Image.asset(imageAssetDefault,
                  fit: fit,
                  width: imageWidth,
                  height: imageWidth,
                  package: packageName),
              onTap: (isOnTapReload)
                  ? () {
                      state.reLoadImage();
                    }
                  : null,
            );
          default:
            return Image.asset(imageAssetDefault,
                fit: fit,
                width: imageWidth,
                height: imageWidth,
                package: packageName);
        }
      },
    );
  }
}
