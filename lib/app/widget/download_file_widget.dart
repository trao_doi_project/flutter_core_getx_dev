import 'package:flutter_core_getx_dev/app/services/core_up_download_service/up_download_package.dart';
import 'package:flutter_core_getx_dev/app/services/media_picker.dart';
import 'package:get/route_manager.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';

import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';

class DownLoadFileWidget extends StatefulWidget {
  final String linkFile;
  final String fileName;
  final Color buttonColor;
  final Color iconColor;
  final double buttonSize;
  final double iconSize;

  const DownLoadFileWidget({
    required this.linkFile,
    required this.fileName,
    this.buttonSize = 50,
    this.iconSize = 30,
    this.buttonColor = Colors.blueAccent,
    this.iconColor = Colors.white,
  });

  @override
  _DownLoadFileWidgetState createState() => _DownLoadFileWidgetState();
}

class _DownLoadFileWidgetState extends State<DownLoadFileWidget> {
  final PublishSubject<Map<String, dynamic>> downloadFile =
      PublishSubject<Map<String, dynamic>>();

  final PublishSubject<double> _progress = PublishSubject<double>();
  String _filePathLocal = '';

  @override
  Widget build(BuildContext context) {
    return Center(
      child: StreamBuilder(
          stream: downloadFile.stream,
          builder: (context, AsyncSnapshot<Map<String, dynamic>> snapshot) {
            if (snapshot.hasData && snapshot.data!['isDownLoad']) {
              return MaterialButton(
                height: widget.buttonSize,
                minWidth: widget.buttonSize,
                onPressed: () async {
                  await MediaPicker.instance
                      .openFile(snapshot.data!['linkFile']);
                },
                color: widget.buttonColor,
                textColor: Colors.white,
                child: const Text(
                  'Mở',
                  style: TextStyle(fontSize: 12),
                ),
                shape: const CircleBorder(),
              );
            } else
              return StreamBuilder(
                  stream: _progress.stream,
                  builder: (context, AsyncSnapshot<double> snapshotProgress) {
                    if (snapshotProgress.hasData) {
                      return CircularPercentIndicator(
                        radius: widget.buttonSize,
                        lineWidth: 5.0,
                        percent: snapshotProgress.data! / 100,
                        center: Text('${snapshotProgress.data!.toInt()}%'),
                        progressColor: widget.buttonColor,
                      );
                    } else
                      return MaterialButton(
                        height: widget.buttonSize,
                        minWidth: widget.buttonSize,
                        onPressed: () {
                          _downloadFile();
                        },
                        color: widget.buttonColor,
                        textColor: widget.iconColor,
                        child: Icon(
                          Icons.file_download,
                          size: widget.iconSize,
                        ),
                        shape: const CircleBorder(),
                      );
                  });
          }),
    );
  }

  Future _checkDowload() async {
    final CreateFolderModel _filePath = await DownloadFile.instance
        .checkAndCreateCacheFolder(widget.fileName, appFolder: 'chat');
    // final String filePath = join(sysPath.path, widget.fileName);
    if (_filePath.isExists) {
      _filePathLocal = _filePath.filePath;
      if (!downloadFile.isClosed)
        downloadFile.sink
            .add({'isDownLoad': true, 'linkFile': _filePath.filePath});
    } else {
      _filePathLocal = _filePath.filePath;
      if (!downloadFile.isClosed)
        downloadFile.sink
            .add({'isDownLoad': false, 'linkFile': widget.linkFile});
    }
  }

  Future _downloadFile() async {
    final String? data = await DownloadFile.instance.downLoadFile(
        widget.linkFile, _filePathLocal,
        callBack: showDownloadProgress);
    if (data != null) {
      if (!downloadFile.isClosed)
        downloadFile.sink.add({'isDownLoad': true, 'linkFile': _filePathLocal});
      await MediaPicker.instance.openFile(_filePathLocal);
    } else {
      Get.snackbar('Thông báo', 'File lỗi hoặc File không tồn tại!!');
      if (!downloadFile.isClosed)
        downloadFile.sink
            .add({'isDownLoad': false, 'linkFile': widget.linkFile});
    }
  }

  void showDownloadProgress(received, total) {
    if (total > 0) {
      final double calPercent = (received / total * 100);
      if (!_progress.isClosed) _progress.sink.add(calPercent);
    } else if (!_progress.isClosed) _progress.sink.add(100.0);
  }

  @override
  void initState() {
    _checkDowload();
    super.initState();
  }

  @override
  void dispose() {
    downloadFile.close();
    _progress.close();
    super.dispose();
  }
}
