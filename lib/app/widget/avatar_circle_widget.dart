import 'package:flutter/material.dart';
import 'package:flutter_core_getx_dev/app/widget/image_network_widget.dart';

//sẽ xóa sau khi xóa app
class AvatarCircleWidget extends StatelessWidget {
  final bool online;
  final double size;
  final BorderRadius? borderRadius;
  final ImageNetworkWidget imageNetworkWidget;
  final double positionDotOnline;
  final double sizeDotOnline;

  const AvatarCircleWidget(
      {required this.imageNetworkWidget,
      this.online = false,
      this.size = 50,
      this.positionDotOnline = 5,
      this.borderRadius,
      this.sizeDotOnline = 10});

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        SizedBox(
          height: size,
          width: size,
          child: ClipRRect(
              borderRadius: borderRadius ?? BorderRadius.circular(75),
              child: imageNetworkWidget),
        ),
        if (online)
          Positioned(
            bottom: positionDotOnline,
            right: positionDotOnline,
            child: ClipRRect(
              child: Container(
                decoration: const BoxDecoration(
                  color: Color(0xff64dd17),
                  shape: BoxShape.circle,
                ),
                width: sizeDotOnline,
                height: sizeDotOnline,
              ),
            ),
          )
      ],
    );
  }
}
